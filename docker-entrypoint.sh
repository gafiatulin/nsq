#!/bin/sh
NSQ_TCP_PORT="${NSQ_TCP_PORT:=4150}"
NSQ_HTTP_PORT="${NSQ_HTTP_PORT:=4151}"

NSQ_LOOKUP_TCP_PORT="${NSQ_LOOKUP_TCP_PORT:=4160}"
NSQ_LOOKUP_HTTP_PORT="${NSQ_LOOKUP_HTTP_PORT:=4161}"

NSQ_ADMIN_HTTP_PORT="${NSQ_ADMIN_HTTP_PORT:=4171}"

nohup nsqlookupd --tcp-address="0.0.0.0:$NSQ_LOOKUP_TCP_PORT" --http-address="0.0.0.0:$NSQ_LOOKUP_HTTP_PORT" &
nohup nsqd --tcp-address="0.0.0.0:$NSQ_TCP_PORT" --http-address="0.0.0.0:$NSQ_HTTP_PORT" --lookupd-tcp-address="0.0.0.0:$NSQ_LOOKUP_TCP_PORT" &
nohup nsqadmin --http-address="0.0.0.0:$NSQ_ADMIN_HTTP_PORT" --lookupd-http-address="0.0.0.0:$NSQ_LOOKUP_HTTP_PORT"
